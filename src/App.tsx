import React, {FC, useEffect} from 'react';
import './App.less';
import { Router, Route } from "react-router-dom"
import axios from "axios";
import {history} from "./services/history";
import SignupPage from "./pages/register";
import Users from "./pages/users";
import Information from "./pages/informations";


const App: FC = () => {

    return (
            <Router history={history}>
                <Route path={'/'} exact component={SignupPage}/>
                <Route path={'/edit/:id'} exact component={SignupPage}/>
                <Route path={'/users'} exact component={Users}/>
                <Route path={'/information'} exact component={Information}/>


            </Router>
    )
}
export default App;
