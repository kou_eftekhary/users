import { createServer, Model } from 'miragejs'

createServer({
    models: {
        user: Model,
        info: Model,

    },
    // @ts-ignore
    users: [],
    infos: [],


    seeds (server) {
        server.create('user', {
            // @ts-ignore
            id: 1,
            name: 'کوروش افتخاری',
            email: 'admin@ad.com',
            phone: '123456',
            age :'20'
        })
        server.create('info', {
            // @ts-ignore
            id: 1,
            name: 'کوروش افتخاری',
            email: 'admin@ad.com',
            phone: '123456',
            date :'2021-07-22T05:57:28.623Z'
        })
    },
    routes () {
        this.namespace = 'api'

        //USER------------------------------------------------
        this.get('/users', (schema, request) => {
            // @ts-ignore
            return schema.users.all()
        })
        this.get('/users/:id', (schema, request) => {
            const id = request.params.id
// @ts-ignore
            return schema.users.find(id)
        })
        this.patch('/users/:id', (schema, request) => {
            const newAttrs = JSON.parse(request.requestBody)
            const id = request.params.id
            //@ts-ignore
            const user = schema.users.find(id)
            return user.update(newAttrs)
        })
        this.delete('/users/:id', (schema, request) => {
            const id = request.params.id
// @ts-ignore
            return schema.users.find(id).destroy()
        })
        this.post('/users', (schema, request) => {
            const attrs = JSON.parse(request.requestBody)
// @ts-ignore
            return schema.users.create(attrs)
        })
        //----------------------------------------------------
        //INFORMATION------------------------------------------------
        this.get('/infos', (schema, request) => {
            // @ts-ignore
            return schema.infos.all()
        })
        this.get('/infos/:id', (schema, request) => {
            const id = request.params.id
// @ts-ignore
            return schema.infos.find(id)
        })
        this.patch('/infos/:id', (schema, request) => {
            const newAttrs = JSON.parse(request.requestBody)
            const id = request.params.id
            //@ts-ignore
            const user = schema.infos.find(id)
            return user.update(newAttrs)
        })
        this.delete('/infos/:id', (schema, request) => {
            const id = request.params.id
// @ts-ignore
            return schema.infos.find(id).destroy()
        })
        this.post('/infos', (schema, request) => {
            const attrs = JSON.parse(request.requestBody)
// @ts-ignore
            return schema.infos.create(attrs)
        })
        //----------------------------------------------------

    }
})
